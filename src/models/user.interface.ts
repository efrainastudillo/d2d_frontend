export interface IUser {
  status: number;
  message: string;
  id: string;
  token: string;
}
