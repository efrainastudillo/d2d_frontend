import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatInputModule } from '@angular/material/input';
import { MatDividerModule } from '@angular/material/divider';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMomentDateModule, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';

import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { SignupComponent } from './signup/signup.component';
import { FooterComponent } from './footer/footer.component';
import { PruebaComponent } from './prueba/prueba.component';
import { HomeComponent } from './home/home.component';
import { PartnersComponent } from './partners/partners.component';
import { PartnerComponent } from './partners/partner/partner.component';

import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShipmentComponent } from './shipment/shipment.component';

import { OnewayComponent } from './shipment/oneway/oneway.component';
import { AdminComponent } from './admin/admin.component';

@NgModule({
   declarations: [
      AppComponent,
      ToolbarComponent,
      LoginComponent,
      SignupComponent,
      FooterComponent,
      PruebaComponent,
      HomeComponent,
      PartnersComponent,
      PartnerComponent,
      ShipmentComponent,
      OnewayComponent,
      AdminComponent
   ],
   imports: [
      BrowserModule,
      BrowserAnimationsModule,
      FontAwesomeModule,
      MatToolbarModule,
      MatIconModule,
      FlexLayoutModule,
      MatCardModule,
      MatInputModule,
      MatButtonModule,
      AppRoutingModule,
      HttpClientModule,
      MatDividerModule,
      FormsModule,
      ReactiveFormsModule,
      MatTabsModule,
      MatExpansionModule,
      MatDatepickerModule,
      MatMomentDateModule
   ],
   providers: [
      {
        provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: {useUtc: true}
      }
    ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
