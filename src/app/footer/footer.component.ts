import { Component, OnInit } from '@angular/core';
import { faTwitter, faFacebook, faInstagram, faGithub, faConnectdevelop, faWordpress } from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  twitterIcon = faTwitter;
  facebookIcon = faFacebook;
  instagramIcon = faInstagram;
  githubIcon = faGithub;
  connectIcon = faConnectdevelop;
  wordpressIcon = faWordpress;

  constructor() { }

  ngOnInit(): void { }
}
