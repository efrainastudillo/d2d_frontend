import { Component, OnInit } from '@angular/core';
import { faTwitter, faFacebook, faInstagram, faGithub, faConnectdevelop, faWordpress } from '@fortawesome/free-brands-svg-icons';
import { faGlobe } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {
  twitterIcon = faTwitter;
  languageIcon = faGlobe;
  facebookIcon = faFacebook;
  instagramIcon = faInstagram;
  githubIcon = faGithub;
  connectIcon = faConnectdevelop;
  wordpressIcon = faWordpress;

  status = false;

  constructor() { }

  ngOnInit() {
  }

  clickEvent(event) {
    console.log('"Click....."');
    this.status = !this.status;
  }
}
