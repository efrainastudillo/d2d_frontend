import { Component, OnInit, OnDestroy } from '@angular/core';
import { faUser, faKey } from '@fortawesome/free-solid-svg-icons';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, NgForm } from '@angular/forms';
import { IUser } from '../../models/user.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  styleUrls: ['./login.component.scss'],
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit, OnDestroy {
  private static TEST_VAR = 0;

  /**
   * Icons for Login fields
   */
  userIcon = faUser;
  passwordIcon = faKey;
  constructor(private http: HttpClient, private router: Router) {

  }

  ngOnDestroy() {
    console.log(`Login component destroyed! ${LoginComponent.TEST_VAR}`);
  }

  ngOnInit() {
    LoginComponent.TEST_VAR++;
    console.log(`Login component created! ${LoginComponent.TEST_VAR}`);
  }

  OnLogin(form: NgForm ) {
    console.log(`Logged In... ${JSON.stringify(form.value)}`);
    const options = {};
    const body = form.value;
    this.http.post<IUser>('http://localhost:8080/api/login', body, options).subscribe( data => {
      console.log(`'Request success!!' : ${data.message} userid: ${data.id} token: ${data.token}`);
    // this.router.navigate()
    }, err => {
      console.log(`Error request POST:  ${err}`);

    });
  }
}
