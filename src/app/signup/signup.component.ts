import { Component, OnInit } from '@angular/core';
import { faTwitter, faFacebook, faInstagram, faGithub, faConnectdevelop, faWordpress } from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  twitterIcon = faTwitter;
  facebookIcon = faFacebook;
  instagramIcon = faInstagram;
  githubIcon = faGithub;
  connectIcon = faConnectdevelop;
  wordpressIcon = faWordpress;

  constructor() { }

  ngOnInit(): void { }
}
