import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-partner',
  templateUrl: './partner.component.html',
  styleUrls: ['./partner.component.scss']
})
export class PartnerComponent implements OnInit {

  @Input()
  bannerImage = 'assets/images/noimage.jpeg';
  @Input()
  profileImage = 'assets/images/noimage-profile.png';

  constructor() { }

  ngOnInit(): void { }
}
